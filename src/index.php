<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href='https://fonts.googleapis.com/css?family=Noto Sans Mono' rel='stylesheet'>
    <link rel="icon" href="data:image/svg+xml,<svg xmlns=%22http://www.w3.org/2000/svg%22 viewBox=%220 0 100 100%22><text y=%22.9em%22 font-size=%2290%22>👀</text></svg>">
    <title>Abstract Wikipedia Reviewer</title>
    <style>
        body,
        html,
        main {
            height: 100%;
            font-family: 'Noto Sans Mono';
            background-color: #222;
            color: #ddd;
        }
        main {
            display: flex;
            align-items: center;
            justify-content: center;
        }
        main #reviewer {
            font-size: 25vmin;
        }
        #nav {
            margin: 0;
            padding: 0;
            position: absolute;
            top: 5px;
            right: 10px;
        }
        #nav a{
            color: #ddd;
        }
    </style>
</head>

<body>
    <main>
        <div id="nav">
            <a href="https://gitlab.wikimedia.org/repos/abstract-wiki/aw-reviewers">source</a>
        </div>
        <div id="reviewer">
            <?php
            $input = array("Allan", "Denny", "James", "Geno", "Cory", "David", "Julia", "Stef");
            $rand_key = array_rand($input, 1);
            echo $input[$rand_key];
            ?>
        </div>
    </main>
</body>

</html>
