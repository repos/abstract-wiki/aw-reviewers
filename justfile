user := `whoami`

run:
  php -S localhost:8000 -t src

deploy toolforge_user=user:
  scripts/deploy_to_toolforge.sh {{toolforge_user}}
